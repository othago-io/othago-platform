/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.examples

import io.othago.platform.common.lang.LST
import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.application.ApplicationBuilder
import io.othago.platform.core.application.ApplicationInstance
import io.othago.platform.core.application.resDscr
import io.othago.platform.core.cqrs.ICommandHandler
import io.othago.platform.core.cqrs.IProcessingContextProvider
import io.othago.platform.core.cqrs.IRepositoryEventHandler
import io.othago.platform.core.cqrs.StateContext
import io.othago.platform.core.env.*
import io.othago.platform.core.security.SecurityContext
import io.othago.platform.core.transaction.TransactionContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import testAppInstance
import java.util.concurrent.atomic.AtomicBoolean

/* Test show how to override components / frame objects in custom app */
/* Custom App */
object CustomComponent : IPlatformComponent {
    override fun retId(): String = "CustomComponent"
}

class CustomExampleContext(
    override val env: CustomExampleEnvironment, //so as not to make env generic in the EnvProcessingContext
    secCtx: SecurityContext,
    txCtx: TransactionContext,
    stateCtx: StateContext,
    val navigationPath: String //context enhancement
) : EnvProcessingContext<CustomExampleContext>(env, secCtx, txCtx, stateCtx)

class CustomExampleContextProvider(navigationRoot: String) : AbstractEnvProcessingContextProvider<CustomExampleContext, CustomExampleEnvironment>(
    ctxFactory = { procCtx, secCtx, txCtx, stateCtx -> CustomExampleContext(procCtx, secCtx, txCtx, stateCtx, navigationRoot) })

class CustomExampleEnvironment(
    procCtxProvider: IProcessingContextProvider<CustomExampleContext>,
    val customComponent: CustomComponent = CustomComponent //environment enhancement
) : ProcessingEnvironment<CustomExampleContext>(procCtxProvider, ProcEnvComponentProvider()) {
    init {
        registerComponent(customComponent)
    }
}

//env builder can be overridden to create custom procProvider or provide DI env factory
class CustomExampleEnvironmentBuilder(val navigationRoot: String) : IEnvironmentBuilder<CustomExampleContext, CustomExampleEnvironment> {
    override fun build(appInstance: ApplicationInstance): CustomExampleEnvironment {
        val procCtxProvider = CustomExampleContextProvider(navigationRoot)
        val env = CustomExampleEnvironment(procCtxProvider)
        procCtxProvider.injectProcEnv(env)
        return env
    }
}

class CustomExampleApplicationBuilder(procEnvBuilder: IEnvironmentBuilder<CustomExampleContext, CustomExampleEnvironment>) :
    ApplicationBuilder<CustomExampleContext, CustomExampleEnvironment>(procEnvBuilder)

/* Custom App Resources */
object CustomExampleCE : CommandEvent()
object CustomExampleRE : RepositoryEvent()
class CustomExampleReactRE(val content: String) : RepositoryEvent()

object CustomExampleCEH : ICommandHandler<CustomExampleContext, CustomExampleCE> {
    override fun handleEvent(): CustomExampleContext.(CustomExampleCE) -> Unit = { state result CustomExampleRE }
}

object CustomExampleREH : IRepositoryEventHandler<CustomExampleContext, CustomExampleRE> {
    var execFlag = AtomicBoolean(false)

    override fun handleEvent(): CustomExampleContext.(CustomExampleRE) -> Unit {
        return {
            execFlag.set(true)
            state result CustomExampleReactRE(CustomComponent.retId())
        }
    }
}

class CustomAppExampleTest {

    @Test
    fun bootstrapAppTest() {

        val customExampleAppBuilder = CustomExampleApplicationBuilder(CustomExampleEnvironmentBuilder("http://EXAMPLE_ROOT"))
        customExampleAppBuilder.registerApplicationResources { resDscr(LST(CustomExampleCEH), LST(CustomExampleREH)) }

        val env = customExampleAppBuilder.createApplication(testAppInstance())

        assertEquals(env.procCtxProvider.createProcessingContext().navigationPath, "http://EXAMPLE_ROOT")

        val res = env.commandEntryPoint.executeCommand(CustomExampleCE)

        assertEquals(2, res.payload.size)
        assertTrue(res.payload[0] is CustomExampleRE)
        assertTrue(CustomExampleREH.execFlag.get())
        assertTrue(res.payload[1] is CustomExampleReactRE)
        assertEquals((res.payload[1] as CustomExampleReactRE).content, CustomComponent.retId())
    }
}
