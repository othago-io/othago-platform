/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.exception.InfrastructureValidationAssert
import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.deploy.PlatformContext
import io.othago.platform.core.deploy.PlatformEnvironment
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import procEnv

class CommandDispatcherTest {

    private lateinit var env: PlatformEnvironment

    @BeforeEach
    fun initTest() {
        env = procEnv()
    }

    @Test
    fun commandDispatcherBaseTest() {
        env.commandHandlerRegistry.registerResource(TestInitCommandCEH)

        val testInitCE = TestInitCE("SampleCommand")
        val res = env.commandDispatcher.dispatchEvent(env.procCtxProvider.createProcessingContext(), testInitCE)

        assertEquals(1, res.size)
        assertTrue(res[0] is TestRE)
        assertEquals((res[0] as TestRE).content, "Init Command RE [SampleCommand]")
    }

    @Test
    fun commandDispatcherReactTest() {
        env.commandHandlerRegistry.registerResource(TestInitCommandCEH)
        env.repositoryEventHandlerRegistry.registerResource(TestBasicReactREH)

        val testCE = TestInitCE("SampleCommand")
        val res = env.commandDispatcher.dispatchEvent(env.procCtxProvider.createProcessingContext(), testCE)

        assertEquals(2, res.size)
        assertTrue(res[0] is TestRE)
        assertEquals((res[0] as TestRE).content, "Init Command RE [SampleCommand]")
        assertTrue(res[1] is TestBasicReactRE)
        assertEquals((res[1] as TestBasicReactRE).content, "Basic React RE [X_VAL]")
    }

    @Test
    fun commandDispatcherReactCommandTest() {
        env.commandHandlerRegistry.registerResource(TestInitCommandCEH)
        val testReactCommandCreateHandler = TestCreateReactCommandREH()
        env.repositoryEventHandlerRegistry.registerResource(testReactCommandCreateHandler)
        env.commandHandlerRegistry.registerResource(TestReactCommandCEH)

        val testCE = TestInitCE("SampleCommand")
        val res = env.commandDispatcher.dispatchEvent(env.procCtxProvider.createProcessingContext(), testCE)

        assertEquals(2, res.size)
        assertTrue(res[0] is TestRE)
        assertEquals((res[0] as TestRE).content, "Init Command RE [SampleCommand]")
        assertTrue(res[1] is TestReactCommandRE)
        assertEquals((res[1] as TestReactCommandRE).content, "React Command RE [Y_VAL]")
    }

    @Test
    fun commandDispatcherFullCommandTest() {
        env.commandHandlerRegistry.registerResource(TestInitCommandCEH)
        env.repositoryEventHandlerRegistry.registerResource(TestBasicReactREH)

        val testReactCommandCreateHandler = TestCreateReactCommandREH()
        env.commandHandlerRegistry.registerResource(TestReactCommandCEH)
        env.repositoryEventHandlerRegistry.registerResource(testReactCommandCreateHandler)
        env.repositoryEventHandlerRegistry.registerResource(TestReactCommandREH)

        /* test ordering */
        env.repositoryEventHandlerRegistry.registerResource(TestBasicReactFinalREH)

        val testCE = TestInitCE("SampleCommand")
        val res = env.commandDispatcher.dispatchEvent(env.procCtxProvider.createProcessingContext(), testCE)

        assertEquals(5, res.size)
        assertTrue(res[0] is TestRE)
        assertEquals((res[0] as TestRE).content, "Init Command RE [SampleCommand]")
        assertTrue(res[1] is TestBasicReactFinalRE) //same level as TestBasicReactRE (parallel exec possible, no ordering constraints, sort by handler id
        assertEquals((res[1] as TestBasicReactFinalRE).content, "Basic React Final RE [X_VAL]")
        assertTrue(res[2] is TestBasicReactRE)
        assertEquals((res[2] as TestBasicReactRE).content, "Basic React RE [X_VAL]")
        assertTrue(res[3] is TestReactCommandRE)
        assertEquals((res[3] as TestReactCommandRE).content, "React Command RE [Y_VAL]")
        assertTrue(res[4] is TestReactCommandReactRE)
        assertEquals((res[4] as TestReactCommandReactRE).content, "React Command React RE [Z_VAL]")
    }

}

// use assert or 3rd party tools like valiktor to validate event
fun TestInitCE.validate() = InfrastructureValidationAssert.assertFalse(content.isBlank(), "content is blank")

class TestInitCE(val content: String) : CommandEvent()

class TestRE(val content: String) : RepositoryEvent()
class TestBasicReactRE(val content: String) : RepositoryEvent()
class TestReactCommandCE(val content: String) : CommandEvent()
class TestReactCommandRE(val content: String) : RepositoryEvent()
class TestReactCommandReactRE(val content: String) : RepositoryEvent()
class TestBasicReactFinalRE(val content: String) : RepositoryEvent()

object TestInitCommandCEH : ICommandHandler<PlatformContext, TestInitCE> {

    override fun handleEvent(): PlatformContext.(TestInitCE) -> Unit {
        return { e ->
            state["X"] = "X_VAL"
            state result TestRE("Init Command RE [${e.content}]")
        }

    }
}

object TestBasicReactREH : IRepositoryEventHandler<PlatformContext, TestRE> {

    override fun handleEvent(): PlatformContext.(TestRE) -> Unit {
        return {
            state result TestBasicReactRE("Basic React RE [${state.accuState.last().state["X"]}]")
        }
    }
}

class TestCreateReactCommandREH : IRepositoryEventHandler<PlatformContext, TestRE> {

    override fun handleEvent(): PlatformContext.(TestRE) -> Unit {
        return { e ->
            state["Y"] = "Y_VAL"
            executeCommand(TestReactCommandCE(e.content))
        }

    }
}

object TestReactCommandCEH : ICommandHandler<PlatformContext, TestReactCommandCE> {

    override fun handleEvent(): PlatformContext.(TestReactCommandCE) -> Unit {
        return {
            state["Z"] = "Z_VAL"
            state result TestReactCommandRE("React Command RE [${state.accuState.last().state["Y"]}]")
        }

    }
}

object TestReactCommandREH : IRepositoryEventHandler<PlatformContext, TestReactCommandRE> {

    override fun handleEvent(): PlatformContext.(TestReactCommandRE) -> Unit {
        return {
            state result TestReactCommandReactRE("React Command React RE [${state.accuState.last().state["Z"]}]")
        }
    }
}

/* test ordering of processing */
object TestBasicReactFinalREH : IRepositoryEventHandler<PlatformContext, TestRE> {

    override fun handleEvent(): PlatformContext.(TestRE) -> Unit {
        return {
            state result TestBasicReactFinalRE("Basic React Final RE [${state.accuState.last().state["X"]}]")
        }
    }
}