/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.deploy

import io.othago.platform.common.lang.LST
import io.othago.platform.core.application.resDscr
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import testAppInstance

class PlatformApplicationTest {

    @Test
    fun bootstrapAppTest() {

        val plfAppBuilder = PlatformApplicationBuilder(PlatformEnvironmentBuilder())
        plfAppBuilder.registerApplicationResources { resDscr(LST(TestCommandP1CEH), LST(TestReactP1REH)) }

        val env = plfAppBuilder.createApplication(testAppInstance())
        val res = env.commandEntryPoint.executeCommand(TestP1CE)

        assertEquals(1, res.payload.size)
        assertTrue(res.payload[0] is TestReactP1RE)
        assertTrue(TestReactP1REH.execFlag.get())
    }
}
