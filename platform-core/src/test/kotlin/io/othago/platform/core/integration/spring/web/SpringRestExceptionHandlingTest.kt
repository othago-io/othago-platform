/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.web

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import kotlin.test.assertTrue

@ExtendWith(SpringExtension::class)
@WebMvcTest(TestRestController::class)
//@WebAppConfiguration
@ComponentScan(basePackages = ["io.othago.platform.core.integration.spring.web"])
//@ContextConfiguration(classes = [SpringBootTestConfig::class])
//@SpringBootTest
@AutoConfigureMockMvc
class SpringRestExceptionHandlingTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    /*var standaloneMvc =  MockMvcBuilders.standaloneSetup(TestRestController())
    .setControllerAdvice(SpringExceptionHandler())
    .build();*/

    @Test
    fun restTest() {
        val result = mockMvc.perform(get("/api/v1/testRestQuery")).andReturn()
        val res = result.response.contentAsString
        assertTrue { res.startsWith("{\"errors") }
    }
}