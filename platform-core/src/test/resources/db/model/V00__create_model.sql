create table ADDRESS
(
    id    NUMBER(10, 0),
    venue VARCHAR2(128),
    PRIMARY KEY (id)
);