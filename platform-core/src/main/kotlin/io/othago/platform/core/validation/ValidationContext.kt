/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.validation

import io.othago.platform.core.api.validation.annotation.*
import io.othago.platform.core.api.validation.constraint.*
import kotlin.reflect.KClass

typealias ConstraintClass = KClass<out Constraint>
typealias IsValid = (Constraint, Any?) -> Boolean
typealias AnnotationClass = KClass<out Annotation>
typealias AnnotationToConstraintMapper = (Annotation) -> Constraint

// TODO jvm reflection dependency

// TODO think of the name different then context

// TODO validation context like json context to be able to customize it per ctx
val validationConstraintValidatorRegistry = mutableMapOf<ConstraintClass, IsValid>(

    NullConstraint::class to { _, v -> v == null },
    NotNullConstraint::class to { _, v -> v != null },
    EqualsConstraint::class to { c, v -> v == null || (c is EqualsConstraint && v == c.value) },
    EqualsIgnoreCaseConstraint::class to { c, v -> v == null || (c is EqualsIgnoreCaseConstraint && v is String && v.equals(c.value, true)) },
    NotEqualsConstraint::class to { c, v -> v == null || (c is EqualsConstraint && v != c.value) },
    NotEqualsIgnoreCaseConstraint::class to { c, v -> v == null || (c is EqualsIgnoreCaseConstraint && v is String && !v.equals(c.value, true)) },
    NotBlankConstraint::class to { _, v -> v == null || v is String && v.isNotBlank() },
    SizeConstraint::class to { c, v -> v == null || (c is SizeConstraint && v is String && v.length in c.min.rangeTo(c.max)) },
    UuidConstraint::class to { _, v -> v == null || (v is String && v.matches("^[a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8}$".toRegex())) },
    LessConstraint::class to { c, v -> v == null || (c is LessConstraint && ((v is Long && v < c.value as Long) || (v is Int && v < c.value as Int))) },
    LessOrEqualConstraint::class to { c, v -> v == null || (c is LessOrEqualConstraint && ((v is Long && v <= c.value as Long) || (v is Int && v <= c.value as Int))) },
    GreaterConstraint::class to { c, v -> v == null || (c is GreaterOrEqualConstraint && ((v is Long && v >= c.value as Long) || (v is Int && v > c.value as Int))) },
    GreaterOrEqualConstraint::class to { c, v -> v == null || (c is GreaterOrEqualConstraint && ((v is Long && v >= c.value as Long) || (v is Int && v >= c.value as Int))) },

    BetweenConstraint::class to { c, v ->
        v == null || (c is BetweenConstraint && (
                (v is Long && (c.start as Long).rangeTo(c.end as Long).contains(v))
                        || (v is Int && (c.start as Int).rangeTo(c.end as Int).contains(v))
                ))
    },
    NotBetweenConstraint::class to { c, v ->
        v == null || (c is BetweenConstraint && (
                (v is Long && !(c.start as Long).rangeTo(c.end as Long).contains(v))
                        || (v is Int && !(c.start as Int).rangeTo(c.end as Int).contains(v))
                ))
    },
    EmptyConstraint::class to { _, v -> v == null || (v is String && v.isEmpty()) },
    NotEmptyConstraint::class to { _, v -> v == null || (v is String && v.isNotEmpty()) },
    BlankConstraint::class to { _, v -> v == null || (v is String && v.isBlank()) },
    InConstraint::class to { c, v -> v == null || (c is InConstraint && c.values.toSet().any { e -> v == e }) },
    InIgnoreCaseConstraint::class to { c, v -> v == null || (c is InIgnoreCaseConstraint && v is String && c.values.toSet().any { e -> v.equals(other = e, ignoreCase = true) }) },
    NotInConstraint::class to { c, v -> v == null || (c is InConstraint && c.values.toSet().none { e -> v == e }) },
    NotInIgnoreCaseConstraint::class to { c, v ->
        v == null || (c is NotInIgnoreCaseConstraint && v is String && c.values.toSet().none { e -> v.equals(other = e, ignoreCase = true) })
    },
    ContainsConstraint::class to { c, v -> true }, // it.contains(value)
    ContainsIgnoreCaseConstraint::class to { c, v -> true }, // it.contains(other = value, ignoreCase = true)
    ContainsAllConstraint::class to { c, v -> true }, // values.all { e -> it.contains(e) }
    ContainsIgnoreCaseAllConstraint::class to { c, v -> true }, //  values.all { e -> it.contains(other = e, ignoreCase = true)
    ContainsAnyConstraint::class to { c, v -> true }, // values.toSet().any { e -> it.contains(e) }
    ContainsIgnoreCaseAnyConstraint::class to { c, v -> true }, // values.any { e -> it.contains(other = e, ignoreCase = true)
    NotContainConstraint::class to { c, v -> true }, // !it.contains(value)
    NotContainIgnoreCaseConstraint::class to { c, v -> true }, // !it.contains(other = value, ignoreCase = true)
    NotContainAllConstraint::class to { c, v -> true }, // !values.toSet().all { e -> it.contains(e) }
    NotContainIgnoreCaseAllConstraint::class to { c, v -> true }, // !values.toSet().all { e -> it.contains(other = e, ignoreCase = true) }
    NotContainAnyConstraint::class to { c, v -> true }, // !values.toSet().any { e -> it.contains(e) }
    NotContainIgnoreCaseAnyConstraint::class to { c, v -> true }, // !values.toSet().any { e -> it.contains(other = e, ignoreCase = true) }
    MatchesConstraint::class to { c, v -> v == null || (c is MatchesConstraint && v is String && v.matches(c.regex)) },
    NotMatchConstraint::class to { c, v -> v == null || (c is MatchesConstraint && v is String && !v.matches(c.regex)) },
    ContainsRegexConstraint::class to { c, v -> true }, // it.contains(regex)
    NotContainRegexConstraint::class to { c, v -> true }, // !it.contains(regex)
    StartsWithConstraint::class to { c, v -> true }, // it.startsWith(prefix)
    StartsWithIgnoreCaseConstraint::class to { c, v -> true }, // it.startsWith(prefix = prefix, ignoreCase = true)
    NotStartWithConstraint::class to { c, v -> true }, // !it.startsWith(prefix)
    NotStartWithIgnoreCaseConstraint::class to { c, v -> true }, // !it.startsWith(prefix = prefix, ignoreCase = true)
    EndsWithConstraint::class to { c, v -> true }, // it.endsWith(suffix)
    EndsWithIgnoreCaseConstraint::class to { c, v -> true }, // it.endsWith(suffix = suffix, ignoreCase = true)
    NotEndWithConstraint::class to { c, v -> true }, // !it.endsWith(suffix)
    NotEndWithIgnoreCaseConstraint::class to { c, v -> true }, // !it.endsWith(suffix = suffix, ignoreCase = true)
    EmailConstraint::class to { c, v -> true }, // it.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$".toRegex()))
    WebsiteConstraint::class to { c, v -> true }, // it.matches("^(https?:\\/\\/)?([a-zA-Z0-9]+(-?[a-zA-Z0-9])*\\.)+[\\w]{2,}(\\/\\S*)?\$".toRegex()))
    InConstraint::class to { c, v -> true }, // values.contains(it)
    NotInConstraint::class to { c, v -> true }, // !values.contains(it)
//    ::class to { c, v -> true }, //
)

fun <T> Comparable<T>.rangeTo(end: Comparable<T>): ClosedRange<*> {
    return when (this) {
        is Long -> this.rangeTo(end as Long)
        else -> throw IllegalArgumentException("Unable to recognize properly type.")
    }
}

val validationAnnotationRegistry = mutableMapOf<AnnotationClass, AnnotationToConstraintMapper>(
    Null::class to { NullConstraint },
    NotNull::class to { NotNullConstraint },
    Equals::class to { EqualsConstraint((it as Equals).value) },
    EqualsIgnoreCase::class to { EqualsIgnoreCaseConstraint((it as EqualsIgnoreCase).value) },
    NotEquals::class to { NotEqualsConstraint((it as NotEquals).value) },
    NotEqualsIgnoreCase::class to { NotEqualsIgnoreCaseConstraint((it as NotEqualsIgnoreCase).value) },
    NotBlank::class to { NotBlankConstraint },
    Size::class to { SizeConstraint((it as Size).min, it.max) },
    Uuid::class to { UuidConstraint },
    Less::class to { LessConstraint((it as Less).value) },
    LessOrEqual::class to { LessOrEqualConstraint((it as LessOrEqual).value) },
    Greater::class to { GreaterConstraint((it as Greater).value) },
    GreaterOrEqual::class to { GreaterOrEqualConstraint((it as GreaterOrEqual).value) },
    Matches::class to { MatchesConstraint((it as Matches).value) }
)
