/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.lang.isNotNull
import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.core.api.cqrs.EventResult
import io.othago.platform.core.api.cqrs.IPlatformEvent
import io.othago.platform.core.api.cqrs.retActionName
import io.othago.platform.core.api.exception.PlatformException
import io.othago.platform.core.api.exception.PlatformGenericException
import io.othago.platform.core.transaction.ITransactionListener

interface IExecutionAspect {
    fun <CTX : ProcessingContext, E : IPlatformEvent, RR : EventResult> execute(ctx: CTX, event: E, block: (CTX, E) -> RR): RR
}

object InvokeAspect : IExecutionAspect {
    override fun <CTX : ProcessingContext, E : IPlatformEvent, RR : EventResult> execute(ctx: CTX, event: E, block: (CTX, E) -> RR): RR = block.invoke(ctx, event)
}

class ExceptionHandlingAspect(private val delegate: IExecutionAspect = InvokeAspect) : IExecutionAspect {

    companion object {
        private val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    override fun <CTX : ProcessingContext, E : IPlatformEvent, RR : EventResult> execute(ctx: CTX, event: E, block: (CTX, E) -> RR): RR {
        try {
            return delegate.execute(ctx, event, block)
        } catch (th: Throwable) {
            when (th) {
                //root ex catch point from processing (where platform keeps control), should throw PlatformEX
                //however there other frameworks above this stack (http / rest etc.), need to map ex in ex handlers there for completness
                is PlatformException -> {
                    // ok situation, ex properly handled
                    log.debug("Unexpected action execution PlatformException", th)
                    if (th.cause.isNotNull()) {
                        log.debug("Exception root cause", th.cause!!)
                    }

                    th.bind("actionName", event.retActionName())
                    throw th
                }
                else -> {
                    // means exception uncaught properly in handler by developer
                    log.error("Unexpected action execution unknown exception", th)
                    throw PlatformGenericException("Unexpected action execution exception", th)
                        .bind("actionName", event.retActionName())
                        .bind("rootEx", th.message ?: "UNKNOWN_EX_MESSAGE")
                }
            }
        }
    }
}

class TxExecutionAspect(val delegate: IExecutionAspect, val txListeners: List<ITransactionListener> = emptyList()) : IExecutionAspect {
    override fun <CTX : ProcessingContext, E : IPlatformEvent, RR : EventResult> execute(ctx: CTX, event: E, block: (CTX, E) -> RR): RR {
        try {
            val res = delegate.execute(ctx, event, block)
            txListeners.forEach { it.beforeCommit(ctx, event, res) }
            ctx.txCtx.commit()
            txListeners.forEach { it.afterCommit(ctx, event, res) }
            return res
        } catch (th: Throwable) {
            txListeners.forEach { it.beforeRollback(ctx, event, th) }
            ctx.txCtx.rollback()
            txListeners.forEach { it.afterRollback(ctx, event, th) }
            throw th
        }
    }
}

