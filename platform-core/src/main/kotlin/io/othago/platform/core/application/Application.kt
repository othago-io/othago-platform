/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.application

import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.common.version.Version
import io.othago.platform.core.env.EnvProcessingContext
import io.othago.platform.core.env.IEnvironmentBuilder
import io.othago.platform.core.env.ProcessingEnvironment


const val DEF_PROFILE = "DEFAULT_PROFILE"

data class ApplicationInstance(
    val name: String,
    val instanceId: String,
    val locale: String,//todo locale -> common, version (module / app) move to common
    val deviceContext: String, // (type[MOBILE | STANDALONE], OS, cpu, RAM)
    val profileId: String = DEF_PROFILE
) {
    override fun toString(): String = "[name=$name,profileId=$profileId,instanceId=$instanceId,locale=$locale,deviceContext=$deviceContext]"
}

/* NOT USED FOR NOW, higher level config concept*/
open class ApplicationDefinition<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>>(
    val name: String = "DEFAULT_APP",
    val version: Version = Version(0, 0, 0)
    //profile Def Map -> profileId : appBuilderFactory (procEnvBuilder) + resources definition
)

abstract class ApplicationBuilder<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>>(
    val procEnvironmentBuilder: IEnvironmentBuilder<CTX, ENV>,
) {
    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    fun sig(): String = "${this::class.simpleName}"

    val bootStepExecList: MutableList<IBootStep<CTX, ENV>> = mutableListOf()

    fun registerBootStep(bootStep: IBootStep<CTX, ENV>) {
        bootStepExecList.add(bootStep)
        log.debug("${sig()}[BootStep registered] ${bootStep::class.simpleName}")
    }

    fun registerApplicationResources(appResProvider: () -> ResourceDescriptor<CTX>) {
        registerBootStep(AppResBootStep(appResProvider))
    }

    fun registerApplicationFeatures(featureDefinitions: List<FeatureDefinition<CTX>>) {
        featureDefinitions.forEach { ftrDef -> registerBootStep(AppFtrBootStep(ftrDef)) }
    }

    fun createApplication(appInstance: ApplicationInstance): ENV {
        log.info("${sig()}[Building ProcessingEnvironment]")
        //Build Processing Environment
        val env = procEnvironmentBuilder.build(appInstance)

        log.info("${sig()}[Bootstrap Application]")
        val bootstrapController = BootstrapController(env, bootStepExecList)
        return bootstrapController.launchBootstrap(appInstance)
    }

}


