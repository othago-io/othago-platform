/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.trasnformer

import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

interface TransformerDefinition<T : Any, R : Any> {
    fun transform(obj: T): R
}

class DefaultTransformerDefinition<T : Any, R : Any>(
    val primConstR: KFunction<R>
) : TransformerDefinition<T, R> {
    companion object {
        fun <T : Any, R : Any> build(clazzT: KClass<T>, clazzR: KClass<R>): TransformerDefinition<T, R> {
            val result = DefaultTransformerDefinition<T, R>(clazzR.primaryConstructor!!)
            result.propsT.putAll(clazzT.memberProperties.associateBy { it.name })
            result.propsR.addAll(result.primConstR.parameters)
            return result
        }
    }

    val propsT = mutableMapOf<String, KProperty1<T, *>>()
    val propsR = mutableListOf<KParameter>()

    override fun transform(obj: T): R {
        val map = propsT.keys.associateWith { propsT[it]?.get(obj) }
        return primConstR.callBy(propsR.associateWith { parse(it, map[it.name]) })
    }

    inline fun parse(param: KParameter, value: Any?): Any? {
        println("matching param ${param.name} of type ${param.type}")
        return if (value == null) null // null handling
        else if (value::class.qualifiedName == param.type.toString()) value // same type handling
        else if (value::class.qualifiedName == "java.util.UUID" && param.type.toString() == "com.benasher44.uuid.Uuid /* = java.util.UUID */") value // TODO hack for UUIDs (how its implemented)
        else throw IllegalArgumentException("Unable to automagically match type ${value::class.qualifiedName} to ${param.type}")
    }
}