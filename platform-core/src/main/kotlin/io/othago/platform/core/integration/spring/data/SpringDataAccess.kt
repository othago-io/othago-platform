/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.data

import io.othago.platform.core.api.cqrs.ConstraintViolation

/*** Constraints ***/
fun <T : Any?> javax.validation.ConstraintViolation<T>.toConstraintViolation(): ConstraintViolation = ConstraintViolation(
    constraintName = this.constraintDescriptor.annotation.annotationClass.simpleName ?: "UNKNOWN_CONSTRAINT",
    dataItemId = "unknown", // TODO can we store this somewhere or customize
    dataItemType = this.rootBeanClass.typeName,
    propertyPath = this.propertyPath.toString(),
    message = this.message,
    invalidValue = this.invalidValue.toString(),
    messageBundle = "NOT_SUPPORTED",
    messageCode = "NOT_SUPPORTED"
)

fun <R> executeWithSpringData(logic: () -> R): R = try {
    logic()
} catch (ex: Throwable) {
    throw SpringDataExceptionTranslator.translateException(ex)
}
