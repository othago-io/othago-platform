/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.entity.IPlatformEntity
import io.othago.platform.core.trasnformer.DefaultTransformerDefinition
import io.othago.platform.core.trasnformer.TransformerDefinition
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation

object TransformerRegistry {
    val transformerDefinitionFactories: MutableMap<String, (KClass<out Any>, KClass<out Any>) -> TransformerDefinition<Any, Any>> = mutableMapOf(
        "default" to { t: KClass<out Any>, r: KClass<out Any> -> DefaultTransformerDefinition.build(t, r) as TransformerDefinition<Any, Any> }
    )
    val transformerDefinitions: MutableMap<String, TransformerDefinition<Any, Any>> = mutableMapOf()
    val transformers: MutableMap<String, ITransformer<Any, Any>> = mutableMapOf()

    fun <T : Any, R : Any> cacheKey(obj: T, clazzR: KClass<R>) = "${if (obj is IPlatformEvent) obj.retActionName() else obj.simpleName()}:${clazzR.simpleName}"
}

inline fun <reified T : Any, reified R : Any> TransformerRegistry.transform(obj: T): R {
    val key = cacheKey(obj, R::class)
    println("transforming ${T::class.qualifiedName} to ${R::class.qualifiedName}")
    when {
        transformers.containsKey(key) -> {
            println("for $key found transformer ${transformers[key]}")
            return transformers[key]!!.transform(obj) as R
        }
        R::class.hasAnnotation<Transformable>() -> {
            val trans = R::class.findAnnotation<Transformable>()!!
            // lets construct one
            if (!transformerDefinitions.containsKey(key)) {
                if (transformerDefinitionFactories[trans.factory] != null) {
                    println("for $key need to with ${trans.factory}) ${transformerDefinitionFactories[trans.factory]} preapre transformerDefinition")
                    transformerDefinitions[key] = transformerDefinitionFactories[trans.factory]!!(T::class, R::class)
                } else {
                    throw IllegalArgumentException("Factory for transformers ${trans.factory} not exists!")
                }
            }
            transformers[key] = ITransformer {
                println("attempting transform $it")
                transformerDefinitions[key]?.transform(it) as R
            }
            return transformers[key]!!.transform(obj) as R
        }
        else -> {
            throw IllegalArgumentException(
                "Can't automagically transform $obj of type ${obj::class.qualifiedName} into ${R::class.qualifiedName}. " +
                        "Might add @Transformable annotation to object for better result."
            )
        }
    }
}

inline fun <reified T : IPlatformEntity, reified R : IResultObject> T.toResultObject(): R = TransformerRegistry.transform(this)
