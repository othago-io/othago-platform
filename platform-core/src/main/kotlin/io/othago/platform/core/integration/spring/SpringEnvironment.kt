/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring

import io.othago.platform.core.application.ApplicationInstance
import io.othago.platform.core.cqrs.CommandEntryPoint
import io.othago.platform.core.cqrs.InvokeAspect
import io.othago.platform.core.cqrs.QueryEntryPoint
import io.othago.platform.core.cqrs.TxExecutionAspect
import io.othago.platform.core.deploy.PlatformContext
import io.othago.platform.core.deploy.PlatformEnvironment
import io.othago.platform.core.env.*
import org.springframework.stereotype.Component

@Component
class SpringTxProcCtxProvider(txCtxProvider: SpringTxContextProvider) :
    AbstractEnvProcessingContextProvider<PlatformContext, PlatformEnvironment>(txContextProvider = txCtxProvider,
        ctxFactory = { procCtx, secCtx, txCtx, stateCtx -> PlatformContext(procCtx, secCtx, txCtx, stateCtx) })

//env builder can be overridden to create custom procProvider or provide DI env factory
@Component
class SpringTxEnvironmentBuilder(val txProcCtxProvider: SpringTxProcCtxProvider) : IEnvironmentBuilder<PlatformContext, PlatformEnvironment> {
    override fun build(appInstance: ApplicationInstance): PlatformEnvironment {
        val env = PlatformEnvironment(txProcCtxProvider, ProcEnvComponentProvider(
            cmdEntryPointFactory = { pcp, cmdd ->
                CommandEntryPoint(
                    DEF_CMD_EP, pcp, cmdd,
                    executionAspect = TxExecutionAspect(InvokeAspect)
                )
            },
            qryEntryPointFactory = { pcp, qed ->
                QueryEntryPoint(
                    DEF_QRY_EP, pcp, qed,
                    // TODO readonly / NEVER different tx behavior for query
                    // query without TX's leave spoiled theads (hanging entity managers)
                    executionAspect = TxExecutionAspect(InvokeAspect)
                )
            }
        ))
        txProcCtxProvider.injectProcEnv(env)
        return env
    }
}
