/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.api.exception.InfrastructureConfigurationException
import io.othago.platform.core.api.exception.PlatformExceptionCode
import io.othago.platform.core.env.IPlatformComponent

open class CommandEventDispatcher<CTX : ProcessingContext>(
    val dispatcherName: String,
    val procCtxProvider: IProcessingContextProvider<CTX>,
    val cmdHandlerRegistry: CommandHandlerRegistry<CTX>,
    val reactDispatcher: RepositoryEventDispatcher<CTX>
) : IPlatformComponent {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    override fun retId(): String {
        return dispatcherName
    }

    private fun <CE : CommandEvent> handleEvent(ctx: CTX, event: CE, block: CTX.(CE) -> Unit): List<RepositoryEvent> {
        ctx.block(event)
        return ctx.stateCtx.retResult()
    }

    open fun <CE : CommandEvent> dispatchEvent(ctx: CTX, event: CE): List<RepositoryEvent> {
        log.debug("${sig()}[BEGIN:Dispatching event][${event.sig()}]")
        try {
            ValidationRegistry.validate(event)

            val res = listOf(cmdHandlerRegistry.retResourceList()
                .first { r -> r.accepts(event) })
                .map {
                    val actualCtx = procCtxProvider.cloneProcessingContext(event.retActionName(), ctx)
                    log.trace("HANDLE[${sig()}][${it.sig()}][${actualCtx.stateCtx.accuState.joinToString(",") { s -> s.actionName }}](${event.retActionName()})")
                    handleEvent(actualCtx, event, it.handleEvent())
                        .map { re -> listOf(re) + reactDispatcher.dispatchEvent(actualCtx, re).toList() }
                        .flatten()
                }.flatten()

            log.debug("${sig()}[END:Dispatching event][${event.sig()}]")

            return res

        } catch (ex: NoSuchElementException) {
            throw throw InfrastructureConfigurationException("${sig()}[EX:Dispatching event][${event.sig()}] Unable to match event handler", PlatformExceptionCode.INF_CONF_DI)
        }

    }

}

open class RepositoryEventDispatcher<CTX : ProcessingContext>(
    val dispatcherName: String,
    val procCtxProvider: IProcessingContextProvider<CTX>,
    val reHandlerRegistry: RepositoryEventHandlerRegistry<CTX>
) : IPlatformComponent {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    override fun retId(): String {
        return dispatcherName
    }

    private fun <RE : RepositoryEvent> handleEvent(ctx: CTX, event: RE, block: CTX.(RE) -> Unit): List<RepositoryEvent> {
        ctx.block(event)
        return ctx.stateCtx.retResult()
    }

    open fun <RE : RepositoryEvent> dispatchEvent(ctx: CTX, event: RE): List<RepositoryEvent> {

        log.debug("${sig()}[BEGIN:Dispatching event][${event.sig()}]")
        val res = reHandlerRegistry.retResourceList()
            .filter { r -> r.accepts(event) }
            .map {
                val newCtx = procCtxProvider.cloneProcessingContext(event.retActionName(), ctx)
                log.trace("HANDLE[${sig()}][${it.sig()}][${newCtx.stateCtx.accuState.joinToString(",") { s -> s.actionName }}](${event.retActionName()})")
                handleEvent(newCtx, event, it.handleEvent())
            }
            .flatten()

        log.debug("${sig()}[END:Dispatching event][${event.sig()}]")
        return res

    }
}

open class QueryEventDispatcher<CTX : ProcessingContext>(
    val dispatcherName: String,
    val qryHandlerRegistry: QueryHandlerRegistry<CTX>
) : IPlatformComponent {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    override fun retId(): String {
        return dispatcherName
    }

    private fun <QE : QueryEvent, R> handleEvent(ctx: CTX, event: QE, block: CTX.(QE) -> R) = ctx.block(event)

    open fun <QE : QueryEvent> dispatchEvent(ctx: CTX, event: QE): Any? {
        log.debug("${sig()}[BEGIN:Dispatching event][${event.sig()}]")

        try {
            ValidationRegistry.validate(event)

            val qHandler = qryHandlerRegistry.retResourceList()
                .first { r -> r.accepts(event) }

            log.trace("HANDLE[${sig()}][${qHandler.sig()}][${ctx.stateCtx.accuState.joinToString(",") { s -> s.actionName }}](${event.retActionName()})")
            val res = handleEvent(ctx, event, qHandler.handleEvent())

            log.debug("${sig()}[END:Dispatching event][${event.sig()}]")
            return res

        } catch (ex: NoSuchElementException) {
            throw throw InfrastructureConfigurationException("${sig()}[EX:Dispatching event][${event.sig()}] Unable to match event handler", PlatformExceptionCode.INF_CONF_DI)
        }

    }
}

open class DirectQueryEventDispatcher<CTX : ProcessingContext>(val dispatcherName: String) : IPlatformComponent {

    override fun retId(): String {
        return dispatcherName
    }

    fun <QE : QueryEvent, R : IResultObject> dispatchEvent(procCtx: CTX, queryEvent: QE, queryHandler: IQueryHandler<CTX, QE, R>): R {
        val handleFunction = queryHandler.handleEvent()
        return procCtx.handleFunction(queryEvent)
    }

}
