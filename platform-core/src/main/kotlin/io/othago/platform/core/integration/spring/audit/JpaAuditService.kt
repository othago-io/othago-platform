/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.audit

import com.benasher44.uuid.Uuid
import io.othago.platform.core.api.audit.AuditEntry
import io.othago.platform.core.api.cqrs.DataItemProto
import io.othago.platform.core.api.cqrs.DataItemRE
import io.othago.platform.core.api.cqrs.PageRequest
import io.othago.platform.core.audit.IAuditService
import io.othago.platform.core.integration.spring.data.toPageable
import io.othago.platform.core.integration.spring.serialization.othagoJson
import io.othago.platform.core.security.UserContext
import io.othago.platform.core.sequence.ISequenceService
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class JpaAuditService(val sequenceService: ISequenceService, val repository: AuditEntryRepository) : IAuditService {

    companion object {
        private val log = LoggerFactory.getLogger(this::class.qualifiedName!!)
    }

    override fun captureDataItemRE(
        user: UserContext,
        txId: String,
        rootActionName: String,
        dataItemRE: DataItemRE
    ) {

        val changeSet = JsonArray(dataItemRE.delta.map {
            othagoJson.encodeToJsonElement(
                AuditEntryCrumbEntity(
                    fieldName = it.fieldName,
                    oldValue = it.initialValue,
                    newValue = it.updatedValue
                )
            )
        })

        val entry = AuditEntryEntity(
            id = sequenceService.uuid(),
            createdById = user.id,
            txId = txId,
            rootActionName = rootActionName,
            execActionName = dataItemRE.executedActionName,
            dataItemProto = dataItemRE.dataItemProto.ordinal,
            dataItemType = dataItemRE.dataItemType,
            dataItemId = dataItemRE.id,
            dataItemVersion = dataItemRE.version,
            changeType = dataItemRE.changeType.ordinal,
            changeDetails = changeSet
        )

        log.info("Delta to save ${dataItemRE.delta}")
        val savedEntry = repository.saveAndFlush(entry)
        log.info("<${savedEntry.id}> ${savedEntry}")
    }

    override fun getEntriesForDataItem(
        dataItemType: String,
        dataItemid: Uuid,
        pageRequest: PageRequest?,
        dataItemProto: DataItemProto
    ): Page<AuditEntry> {
        return repository.findByDataItemTypeAndDataItemIdAndDataItemProto(
            dataItemType,
            dataItemid,
            dataItemProto.ordinal,
            pageRequest?.toPageable()
        ).map { it.toAuditEntry() }
    }
}