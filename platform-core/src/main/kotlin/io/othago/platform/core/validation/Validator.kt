/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.validation

import io.othago.platform.core.api.cqrs.ConstraintViolation
import io.othago.platform.core.api.validation.constraint.Constraint
import io.othago.platform.core.cqrs.objectIdentity
import io.othago.platform.core.cqrs.objectType
import kotlin.reflect.KProperty1

// TODO jvm reflection dependency

open class Validator<E>(private val obj: E) {

    val constraintViolations = mutableSetOf<ConstraintViolation>()

    @JvmName("validate")
    fun <T> validate(property: KProperty1<E, T?>): Property<T?> = Property(property)

    fun <T> validate(property: KProperty1<E, T?>, block: Property<T?>.() -> Unit): Property<T?> {
        return validate(property).apply { block(this) }
    }

    @JvmName("validateIterable")
    fun <T> validate(property: KProperty1<E, Iterable<T>?>): Property<Iterable<T>?> = Property(property)

    @JvmName("validateArray")
    fun <T> validate(property: KProperty1<E, Array<T>?>): Property<Array<T>?> = Property(property)

    open inner class Property<T>(val property: KProperty1<E, T?>) {
        fun validate(constraint: (T?) -> Constraint, isValid: (T?) -> Boolean): Property<T> {
            val value = this.property.get(obj)
            if (!isValid(value)) {
                val cv = ConstraintViolation(
                    constraintName = constraint(value).name,
                    dataItemId = obj!!.objectIdentity(),
                    dataItemType = obj.objectType(),
                    propertyPath = this.property.name,
                    invalidValue = value?.toString(),
                    // TODO i18n support
                    message = "unknown",
                    messageBundle = "unknown",
                    messageCode = "unknown",
                    messageParams = mapOf()
                )
                println("---> constraint violated $cv")
                this@Validator.constraintViolations += cv
            }
            return this
        }

        fun validate(constraint: Constraint, isValid: (T?) -> Boolean): Property<T> =
            validate({ constraint }, isValid)

        fun addConstraintViolations(violatedConstraintViolations: Iterable<ConstraintViolation>) {
            this@Validator.constraintViolations += violatedConstraintViolations
        }
    }
}

