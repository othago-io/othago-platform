/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.env

import io.othago.platform.common.exception.InfrastructureValidationAssert
import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.QueryEvent
import io.othago.platform.core.cqrs.AbstractProcessingContextProvider
import io.othago.platform.core.cqrs.ProcessingContext
import io.othago.platform.core.cqrs.StateContext
import io.othago.platform.core.security.DefaultSecurityContextProvider
import io.othago.platform.core.security.ISecurityContextProvider
import io.othago.platform.core.security.SecurityContext
import io.othago.platform.core.transaction.DefaultTransactionContextProvider
import io.othago.platform.core.transaction.ITransactionContextProvider
import io.othago.platform.core.transaction.TransactionContext
import kotlin.reflect.full.allSupertypes


/* Environment inner ProcessingContext self ref for convenient access - e.g. repository handlers can dispatch react command event directly - no DI needed */

//ProcessingEnvironment is not generic itself -> conscious decision, lot of burden in all platform defs code,
// env prop must be overridden in descendants to enable access to enhanced env props
open class EnvProcessingContext<CTX : EnvProcessingContext<CTX>>(
    open val env: ProcessingEnvironment<CTX>,
    secCtx: SecurityContext,
    txCtx: TransactionContext,
    stateCtx: StateContext
) : ProcessingContext(secCtx, txCtx, stateCtx) {

    fun <CE : CommandEvent> executeCommand(event: CE) {
        assertContextSelfRef()
        stateCtx.result(env.commandDispatcher.dispatchEvent(@Suppress("UNCHECKED_CAST") (this as CTX), event))
    }

    fun <QE : QueryEvent> executeQuery(event: QE): Any? {
        assertContextSelfRef()
        return env.queryDispatcher.dispatchEvent(@Suppress("UNCHECKED_CAST") (this as CTX), event)
    }

    private fun assertContextSelfRef() {
        //TODO should the check be on types or classes (all supertypeclasses?) check same for handler
        val inheritHierarchy = this::class.allSupertypes.toMutableList()

        //TODO message, expected type and found type (ex binds?)
        InfrastructureValidationAssert.assertTrue(this::class == inheritHierarchy[inheritHierarchy.size - 3].arguments[0].type!!.classifier, "abc")
    }

}

abstract class AbstractEnvProcessingContextProvider<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>>(
    securityContextProvider: ISecurityContextProvider = DefaultSecurityContextProvider,
    txContextProvider: ITransactionContextProvider = DefaultTransactionContextProvider,
    val ctxFactory: (ENV, SecurityContext, TransactionContext, StateContext) -> CTX
) : AbstractProcessingContextProvider<CTX>(securityContextProvider, txContextProvider) {

    lateinit var procEnv: ENV //DI, not thru constructor -> circular dep

    protected fun retProcEnv(): ENV {
        InfrastructureValidationAssert.assertNotNull(procEnv, "Processing Environment should be provided")
        return procEnv
    }

    fun injectProcEnv(env: ENV) {
        procEnv = env
    }

    override fun createProcessingContext(): CTX {
        return ctxFactory.invoke(retProcEnv(), securityContextProvider.provideSecurityContext(), txContextProvider.provideTransactionContext(), StateContext("INIT"))
    }

    override fun cloneProcessingContext(actionName: String, ctx: CTX): CTX {
        return ctxFactory.invoke(retProcEnv(), ctx.secCtx, ctx.txCtx, ctx.stateCtx.clone(actionName))
    }
}