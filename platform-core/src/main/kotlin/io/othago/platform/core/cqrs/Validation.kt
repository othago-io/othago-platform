/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.core.api.cqrs.IPlatformEvent
import io.othago.platform.core.api.cqrs.Validator
import io.othago.platform.core.api.cqrs.retActionName
import io.othago.platform.core.api.cqrs.simpleName
import io.othago.platform.core.api.validation.annotation.Validatable
import io.othago.platform.core.validation.ValidatorDefinition
import io.othago.platform.core.validation.dsl.annotatedValidator
import kotlin.reflect.full.hasAnnotation

// TODO jvm reflection dependency

object ValidationRegistry {
    val validatorDefinitions: MutableMap<String, ValidatorDefinition<Any>> = mutableMapOf()
    val validators: MutableMap<String, Validator<Any>> = mutableMapOf()

    fun <T : Any> cacheKey(obj: T) = if (obj is IPlatformEvent) obj.retActionName() else obj.simpleName()
}

inline fun <T : Any> ValidationRegistry.validate(obj: T?) {
    if (obj == null) return
    println("validating ${obj::class.qualifiedName} (${obj::class.hasAnnotation<Validatable>()})")
    val key = cacheKey(obj)
    if (validators.containsKey(key)) {
        println("for $key found transformer ${validators[key]}")
        validators[key]!!.invoke(obj)
    } else if (obj::class.hasAnnotation<Validatable>()) {
        // lets construct one
        if (!validatorDefinitions.containsKey(key)) {
            println("for $key need to preapre validatorDefinition")
            validatorDefinitions[key] = annotatedValidator(obj::class) as ValidatorDefinition<Any>
        }
        validators[key] = Validator { println("attempting validate $it"); validatorDefinitions[key]?.validate(it) }
        validators[key]!!.invoke(obj)
    }
}
