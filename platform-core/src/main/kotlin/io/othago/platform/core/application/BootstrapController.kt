/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.application

import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.core.env.EnvProcessingContext
import io.othago.platform.core.env.ProcessingEnvironment

open class BootstrapController<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>>(
    val procEnv: ENV,
    val bootStepList: List<IBootStep<CTX, ENV>> = emptyList()
) {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    fun sig(): String = "${this::class.simpleName}"

    fun launchBootstrap(appInstance: ApplicationInstance): ENV {

        log.info("${sig()}[Bootstrap : ApplicationInstance] $appInstance")


        //Phase 1. execute boot step load
        log.info("${sig()}[[Bootstrap : Executing Boot Steps Load Phase]")
        bootStepList.forEach { bpe ->
            log.debug("${sig()}[[BootStep load : start] ${bpe.retBootStepId()}")
            bpe.load(appInstance, procEnv)
            log.debug("${sig()}[[BootStep load : finished] ${bpe.retBootStepId()}")
        }

        //Phase 2. init env -> components should be already injected
        // * DI static app env components
        // * dynamic components from boot steps
        procEnv.envLifecycleManager.init()

        //Phase 3. execute boot step activation
        log.info("${sig()}[[Bootstrap : Executing Boot Steps Activate Phase]")
        bootStepList.forEach { bpe ->
            log.debug("${sig()}[[BootStep activate : start] ${bpe.retBootStepId()}")
            bpe.activate(appInstance, procEnv)
            log.debug("${sig()}[[BootStep activate : finished] ${bpe.retBootStepId()}")
        }

        //Phase 4. trigger Processing Environment lifecycle
        procEnv.envLifecycleManager.start()

        return procEnv
    }
}

interface IBootStep<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>> {

    fun retBootStepId(): String {
        return this::class.simpleName!!
    }

    fun load(appInstance: ApplicationInstance, env: ENV) {}
    fun activate(appInstance: ApplicationInstance, env: ENV) {}
}