/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.data

import io.othago.platform.common.exception.Assert
import io.othago.platform.core.api.exception.*
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DataRetrievalFailureException
import org.springframework.dao.DuplicateKeyException
import org.springframework.orm.ObjectOptimisticLockingFailureException

object SpringDataExceptionTranslator : IExceptionTranslator {

    private val patternJpaObjectRetrievalFailureException = "Unable to find (${Assert.patternClassName}) with id (${Assert.patternUUID});".toRegex()

    override fun translateException(ex: Throwable): PlatformException {
        when (ex) {
            is ObjectOptimisticLockingFailureException -> {
                return InfrastructureDataOLException("Miss match in version for entity", ex)
                    .bind("dataType", ex.persistentClassName)
                    .bind("id", ex.identifier.toString())
            }

            is DataRetrievalFailureException -> {
                val ex = BusinessDataRetrievalException("Unable to locate entity", ex)
                if (!ex.message.isNullOrBlank()) {
                    // val (persistenceClassName,id) = patternJpaObjectRetrievalFailureException.find(ex.message!!)!!.destructured
                    // ex.bind("dataType",persistenceClassName)
                    // ex.bind("id",id)
                }
                return ex
            }

            is DuplicateKeyException -> {
                return BusinessDataIntegrityException("Entry with given id already exists", ex)
            }

            // TODO review from older frameworks
//        catch (DuplicateKeyException uniqEx){
//            String message = uniqEx.getCause().getMessage();
//            Matcher matcher = UNIQ_EX_PATTERN.matcher(message);
//            if(matcher.matches()){
//                String fieldName = matcher.group(1);
//                Pattern fieldValuePattern = Pattern.compile(String.format(FIELD_VALUE_PATTERN, fieldName));
//                Matcher fieldValueMatcher = fieldValuePattern.matcher(message);
//                if (fieldValueMatcher.matches()) {
//                    String fieldValue = fieldValueMatcher.group(1);
//                    throw new PlatformFieldValidationException(fieldName,
//                    String.format("A \"%s\" with value \"%s\" already exists. Please enter a different value", fieldName, fieldValue)
//                    );
//                }
//                throw new PlatformFieldValidationException(fieldName,
//                "Uniq constraint violated for field: "+fieldName+" while executing command for entity: "+e.getClass().getSimpleName()+" [id:"+e.getUid().toHexString()+"]");
//            }else{
//                throw new PlatformValidationException("Uniq constraint violated for entity: "+e.getClass().getSimpleName());
//            }

            is DataIntegrityViolationException -> { //DuplicateKeyException is a subclass
                return BusinessDataIntegrityException("Integrity constraints violated", ex)
            }

            is javax.validation.ConstraintViolationException -> {
                return BusinessDataConstraintsViolationException()
                    .bindConstraintViolations(ex.constraintViolations.map { it.toConstraintViolation() })
            }

            is PlatformException -> return ex

            else -> {
                return InfrastructureDataAccessException("Unexpected data access exception", ex)
            }
        }
    }
}
