/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.validation.dsl

import io.othago.platform.core.api.cqrs.IDataItemEvent
import io.othago.platform.core.api.validation.constraint.*
import io.othago.platform.core.validation.ValidatorDefinition
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

fun <E : Any, T> ValidatorDefinition<E>.PropertyDefinition<T?>.isNotNull() = add(NotNullConstraint)
fun <E : Any, T : String> ValidatorDefinition<E>.PropertyDefinition<T?>.isNotBlank() = add(NotBlankConstraint)
fun <E : Any, T : Comparable<T>> ValidatorDefinition<E>.PropertyDefinition<T?>.isGreaterThanOrEqualTo(value: T) = when (value) {
    is Long -> add(GreaterOrEqualConstraint(value)); else -> {
    }
}

fun <E : Any, T : String> ValidatorDefinition<E>.PropertyDefinition<T?>.matches(regex: Regex) = add(MatchesConstraint(regex))
fun <E : Any, T : String> ValidatorDefinition<E>.PropertyDefinition<T?>.hasSize(min: Int, max: Int) = add(SizeConstraint(min, max))

fun <E : IDataItemEvent> ValidatorDefinition<E>.PropertyDefinition<String?>.isUuid() = add(UuidConstraint)


inline fun <reified E : Any> validator(block: ValidatorDefinition<E>.() -> Unit): ValidatorDefinition<E> = ValidatorDefinition(E::class).apply { block(this) }

fun <E : Any, T> ValidatorDefinition<E>.property(property: KProperty1<E, T?>, block: ValidatorDefinition<E>.PropertyDefinition<T?>.() -> Unit) {
    val oproperty: ValidatorDefinition<E>.PropertyDefinition<T?> = this.PropertyDefinition(property)
    this.apply { block(oproperty) }
    this.propertyList.add(oproperty)
}

/* TODO JVM DEP */
inline fun <reified T : Any> T.asMap(): Map<String, Any?> {
    val props = T::class.memberProperties.associateBy { it.name }
    return props.keys.associateWith { props[it]?.get(this) }
}

inline fun <reified E : Any> annotatedValidator(): ValidatorDefinition<E> = ValidatorDefinition.annotated(E::class)
inline fun <E : Any> annotatedValidator(clazz: KClass<E>): ValidatorDefinition<E> = ValidatorDefinition.annotated(clazz)
