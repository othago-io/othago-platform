/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.transaction

import org.slf4j.LoggerFactory
import java.util.*

interface ITransactionContextProvider {
    fun provideTransactionContext(): TransactionContext
}

object DefaultTransactionContextProvider : ITransactionContextProvider {
    private val log = LoggerFactory.getLogger(this::class.qualifiedName!!)

    override fun provideTransactionContext(): TransactionContext {
        val txContext = TransactionContext(UUID.randomUUID().toString(), DefaultPlatformTxManager, DefaultPlatformTxManager.getTransaction())
        log.info("create transaction context $txContext")
        return txContext
    }

}

interface ITransactionStatus //integration specific tx Object
class DefaultTxStatus : ITransactionStatus

class TransactionContext(val txId: String, val txManager: IPlatformTxManager, val txStatus: ITransactionStatus) {
    companion object {
        private val log = LoggerFactory.getLogger(this::class.qualifiedName!!)
    }

    fun commit() {
        log.info("[${txId}] before commit")
        txManager.commit(txStatus)
        log.info("[${txId}] after commit")
    }

    fun rollback() {
        log.info("[${txId}] before rollback")
        txManager.rollback(txStatus)
        log.info("[${txId}] after rollback")
    }

    fun suspend() {
        txManager.suspend(txStatus)
    }

    fun resume() {
        txManager.resume(txStatus)
    }
}
