/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.IPlatformResource
import io.othago.platform.common.ResourceRegistry
import io.othago.platform.core.api.cqrs.*
import kotlin.reflect.full.allSupertypes

interface BasePlatformResource<out E : IPlatformEvent> : IPlatformResource<String> {
    override fun retId(): String {
        return this::class.qualifiedName!!
    }
}

interface IEventHandler<CTX : ProcessingContext, out E : IPlatformEvent, out R> : BasePlatformResource<E> { //invariance hack / @UnsafeVariance
    fun accepts(event: IPlatformEvent): Boolean { //TODO policies for inheritance handling
        val sTypes = this::class.allSupertypes.toTypedArray()
        return event.retId() == sTypes[sTypes.size - 4].arguments[1].type!!.classifier //exact match no inheritance respecting type matching for now
    }

    fun handleEvent(): CTX.(@UnsafeVariance E) -> R
}

/* returns RepositoryEvent, why such a constraint?
* - mutated state track, explicitly required by api as return (less error prone than calling RE dispatcher by developer decision)
* - react RE handlers result folding - stream flatting vs recursion
* - FE consistent handling - UI gets "change notification" instead of concrete TO which would bind command (result) to particular view (expected TO).
*   FE code can decide to apply change to internal state store (if RE carries enough info) or re-query BE for fresh object state (by ID) */
interface ICommandHandler<CTX : ProcessingContext, out CE : CommandEvent> : IEventHandler<CTX, CE, Unit>

interface IQueryHandler<CTX : ProcessingContext, out QE : QueryEvent, out R : IResultObject> : IEventHandler<CTX, QE, R>
interface IRepositoryEventHandler<CTX : ProcessingContext, out RE : RepositoryEvent> : IEventHandler<CTX, RE, Unit>

open class CommandHandlerRegistry<CTX : ProcessingContext>(registryName: String) : ResourceRegistry<String, ICommandHandler<CTX, CommandEvent>>(registryName)
open class QueryHandlerRegistry<CTX : ProcessingContext>(registryName: String) : ResourceRegistry<String, IQueryHandler<CTX, QueryEvent, IResultObject>>(registryName)
open class RepositoryEventHandlerRegistry<CTX : ProcessingContext>(registryName: String) : ResourceRegistry<String, IRepositoryEventHandler<CTX, RepositoryEvent>>(registryName)