/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.web

import io.othago.platform.core.api.cqrs.ErrorResult
import io.othago.platform.core.api.exception.BusinessDataConstraintsViolationException
import io.othago.platform.core.api.exception.PlatformExceptionCode
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.buildErrorResponse
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.handleAuthError
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.handleError
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.handleServletError
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

interface IErrorResponseMapper {
    fun mapError(referer: String, request: HttpServletRequest, response: HttpServletResponse): ResponseEntity<ErrorResult>?
}

interface IErrorResponseExceptionMapper {
    fun mapException(referer: String, ex: Throwable): ResponseEntity<ErrorResult>?
}

//default one, don't inject with spring DI, direct use
object SpringErrorResponseMapper : IErrorResponseMapper {
    override fun mapError(referer: String, request: HttpServletRequest, response: HttpServletResponse): ResponseEntity<ErrorResult>? {

        if (response.status == HttpStatus.UNAUTHORIZED.value()) {
            return handleAuthError(referer, response)
        } else if (response.status == HttpStatus.FORBIDDEN.value()) {
            return handleError(
                referer, buildErrorResponse(
                    response.status,
                    PlatformExceptionCode.INF_AUTH_FORBIDDEN,
                    "Originator not authorized to access service"
                )
            )
        }
        return handleServletError(referer, request, response)
    }
}

//default one, don't inject with spring DI, direct use
object SpringErrorResponseExceptionMapper : IErrorResponseExceptionMapper {

    override fun mapException(referer: String, ex: Throwable): ResponseEntity<ErrorResult>? {
        return handleError(
            referer, buildErrorResponse(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                PlatformExceptionCode.PLF_GENERIC,
                ex.localizedMessage
            )
        )
    }

}

@Component
class ServletErrorResponseExceptionMapper : IErrorResponseExceptionMapper {

    override fun mapException(referer: String, ex: Throwable): ResponseEntity<ErrorResult>? {
        if (ex is ServletException) {
            return handleError(
                referer, buildErrorResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    PlatformExceptionCode.INF_SERVLET_GENERIC, ex.message ?: "Unknown Servlet Exception"
                )
            )
        }
        return null
    }

}

@Component
class ValidationErrorResponseExceptionMapper : IErrorResponseExceptionMapper {

    override fun mapException(referer: String, ex: Throwable): ResponseEntity<ErrorResult>? {
        return if (ex is BusinessDataConstraintsViolationException) {
            handleError(referer, buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex))
        } else null
    }
}