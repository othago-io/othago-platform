/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.lang

import io.othago.platform.common.exception.PlatformCommonAssert


fun <T : Any?> T?.isNotNull(): Boolean = this != null
fun <T : Any?> T?.isNull(): Boolean = this == null

fun <K : Any, V : Any?, M : Map<K, V>> M.ret(key: K): V {
    PlatformCommonAssert.assertContains(this, key, "Map doesn't contain key : $key")
    return this.getValue(key)
}

inline fun <reified T> LST(vararg elements: T): List<T> = elements.toList()

