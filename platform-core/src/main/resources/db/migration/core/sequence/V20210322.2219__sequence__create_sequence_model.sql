----------------------------------- SERIAL

CREATE TABLE sequences
(
    id    UUID,
    value BIGINT DEFAULT 1,
    type  TEXT NOT NULL,

    CONSTRAINT pk_sequences PRIMARY KEY (id)
);

CREATE INDEX in_sequences_name ON sequences (type);

commit;
