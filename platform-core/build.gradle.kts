plugins {
    kotlin("jvm") apply true
    kotlin("plugin.serialization") apply true
    kotlin("plugin.spring") version "1.4.32"
    kotlin("plugin.jpa") version "1.4.32" apply true
    id("maven-publish") apply true
    id("org.jetbrains.dokka") version "1.4.30" apply true
}

val kotlinVersion: String by project
val kotlinxSerializationVersion: String by project
val kotlinWrappersSuffix: String by project
val springBootVersion: String by project
val junitJupiterEngineVersion: String by project
val keycloakVersion: String by project
val valiKtorVersion: String by project
val kotlinLoggingVersion: String by project
val flywayVersion: String by project
val h2Version: String by project

val pJvmTarget: String by project
val pFreeCompilerArgs: String by project

dependencies {
    api(project(":platform-core-api", "jvmDefault"))
    api(kotlin("stdlib-jdk8"))
    api("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
    api("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
    api("org.springframework.boot:spring-boot-starter-validation:$springBootVersion")
    api("org.springframework.boot:spring-boot-starter-data-jpa:$springBootVersion")
    api("org.springframework.boot:spring-boot-starter-web:$springBootVersion")
    api("org.keycloak:keycloak-adapter-core:$keycloakVersion")
    api("org.keycloak:keycloak-adapter-spi:$keycloakVersion")

    runtimeOnly("org.flywaydb:flyway-core:$flywayVersion")
    runtimeOnly("com.h2database:h2:$h2Version")

    api("org.springframework.boot:spring-boot-starter-test:$springBootVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitJupiterEngineVersion")
    testImplementation("org.springframework.boot:spring-boot-starter-test:$springBootVersion")
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = pJvmTarget
        freeCompilerArgs = listOf(pFreeCompilerArgs)
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        showStandardStreams = true
        events("passed", "skipped", "failed")
    }
}

tasks.withType<org.gradle.jvm.tasks.Jar>().all {
    from(project.projectDir) {
        include("LICENSE.txt", "NOTICE.txt", "README.md")
        into("META-INF")
    }
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("dokka"))
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.getByName("main").allSource)
}
val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    dependsOn(tasks.dokkaHtml)
}

publishing {
    repositories {
        mavenLocal {}
    }
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            pom {
                name.set("Othago Platform Core")
                description.set("Platform for building solutions.")
                url.set("http://othago.io/platform-core")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
            }
            artifact(dokkaJar)
            artifact(sourcesJar)
        }
    }
}
