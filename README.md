# Othago Platform

Copyright (c) 2015-2021 Othago Software (authors: Piotr Migda, Michał Szymański)

This product is licensed to you under the Apache License, Version 2.0
(the "License"). You may not use this product except in compliance with
the License.
