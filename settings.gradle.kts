enableFeaturePreview("GRADLE_METADATA")

pluginManagement {
    repositories {
        maven("https://repo.spring.io/milestone")
        maven("https://repo.spring.io/snapshot")
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
        maven("https://dl.bintray.com/kotlin/kotlin-dev")
        gradlePluginPortal()
        jcenter()
    }
}
rootProject.name = "othago"

include("platform-core-api")
include("platform-core")
