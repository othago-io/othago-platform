/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.api.cqrs

import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
data class PageRequest(
    var page: Int,
    var size: Int,
    var orders: List<OrderRequest> = listOf()
) : IRequestObject {
    companion object {
        fun empty(): PageRequest = PageRequest(0, 10)
        fun of(
            page: Int = 0,
            size: Int = 10 // TODO discover those defaults?
        ): PageRequest = PageRequest(page, size)

        fun of(
            page: Int = 0,
            size: Int = 10,
            vararg orders: Pair<String, SortDirection> // TODO discover those defaults?
        ): PageRequest = PageRequest(page, size, orders.map { OrderRequest(it.first, it.second) })
    }
}

@Serializable
data class OrderRequest(
    var property: String,
    var direction: SortDirection = SortDirection.ASC,
    var ignoreCase: Boolean = false,
    var nullHandling: NullHandling = NullHandling.NATIVE
) : IRequestObject

@Serializable(with = SortDirectionSerializer::class)
enum class SortDirection {
    ASC, DESC
}

@Serializer(forClass = SortDirection::class)  // 1
object SortDirectionSerializer {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor(
            "io.othago.platform.core.api.SortDirectionSerializer",
            PrimitiveKind.STRING
        )

    override fun deserialize(decoder: Decoder): SortDirection {  // 3
        return SortDirection.valueOf(decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, obj: SortDirection) {  // 4
        encoder.encodeString(obj.name)
    }
}

@Serializable(with = NullHandlingSerializer::class)
enum class NullHandling {
    NATIVE, NULLS_FIRST, NULLS_LAST
}

@Serializer(forClass = NullHandling::class)  // 1
object NullHandlingSerializer {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor(
            "io.othago.platform.core.api.NullHandlingSerializer",
            PrimitiveKind.STRING
        )

    override fun deserialize(decoder: Decoder): NullHandling {  // 3
        return NullHandling.valueOf(decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, obj: NullHandling) {  // 4
        encoder.encodeString(obj.name)
    }
}
