/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.api.exception

import io.othago.platform.core.api.cqrs.*

class BusinessDataConstraintsViolationException(
    msg: String = "Constraints violated",
    code: IPlatformExceptionCode = PlatformExceptionCode.BUS_DATA_CONSTRAINT_VIOLATION
) : BusinessDataException(msg, code) {

    private val violations = mutableListOf<ConstraintViolation>()

    fun bindConstraintViolation(cv: ConstraintViolation): BusinessDataConstraintsViolationException {
        violations.add(cv)
        return this
    }

    fun bindConstraintViolations(cvList: Collection<ConstraintViolation>): BusinessDataConstraintsViolationException {
        violations.addAll(cvList)
        return this
    }

    override fun toErrorResult(): ErrorResult = ErrorResult(
        listOf(ErrorItem(code.toResultObject(), message!!, retParamList())) //add root
            .union(violations.map { cv ->
                ErrorItem( //flatten
                    ExceptionCode(cv.messageCode, "Validation Exception"),
                    cv.message,
                    cv.toErrorParamList()
                )
            })
            .toList()
    )
}


///// -- JAVAX
///**
// * Describes a constraint violation. This object exposes the constraint
// * violation context as well as the message describing the violation.
// *
// * @param <T> the type of the root bean
// *
// * @author Emmanuel Bernard
//</T> */
//interface ConstraintViolation<T> {
//    /**
//     * @return the interpolated error message for this constraint violation
//     */
//    val message: String?
//
//    /**
//     * @return the non-interpolated error message for this constraint violation
//     */
//    val messageTemplate: String?
//
//    /**
//     * Returns the root bean being validated. For method validation, returns
//     * the object the method is executed on.
//     *
//     *
//     * Returns `null` when:
//     *
//     *  * the `ConstraintViolation` is returned after calling
//     * [Validator.validateValue]
//     *  * the `ConstraintViolation` is returned after validating a
//     * constructor.
//     *
//     *
//     * @return the validated object, the object hosting the validated element or `null`
//     */
//    val rootBean: T
//
//    /**
//     * Returns the class of the root bean being validated.
//     * For method validation, this is the object class the
//     * method is executed on.
//     * For constructor validation, this is the class the constructor
//     * is declared on.
//     *
//     * @return the class of the root bean or of the object hosting the validated element
//     */
//    val rootBeanClass: Class<T>?
//
//    /**
//     * Returns:
//     *
//     *  * the bean instance the constraint is applied on if it is
//     * a bean constraint
//     *  * the bean instance hosting the property the constraint
//     * is applied on if it is a property constraint or a container element constraint
//     * hosted on a property
//     *  * `null` when the `ConstraintViolation` is returned
//     * after calling [Validator.validateValue]
//     *
//     *  * the object the method is executed on if it is
//     * a method parameter, cross-parameter or return value constraint or a
//     * container element constraint hosted on a method parameter or return value
//     *  * `null` if it is a constructor parameter or
//     * cross-parameter constraint or a container element constraint hosted on a
//     * constructor parameter
//     *  * the object the constructor has created if it is a
//     * constructor return value constraint
//     *
//     *
//     * @return the leaf bean
//     */
//    val leafBean: Any?
//
//    /**
//     * Returns an `Object[]` representing the constructor or method invocation
//     * arguments if the `ConstraintViolation` is returned after validating the
//     * method or constructor parameters.
//     * Returns `null` otherwise.
//     *
//     * @return parameters of the method or constructor invocation or `null`
//     *
//     * @since 1.1
//     */
//    val executableParameters: Array<Any?>?
//
//    /**
//     * Returns the return value of the constructor or method invocation
//     * if the `ConstraintViolation` is returned after validating the method
//     * or constructor return value.
//     *
//     *
//     * Returns `null` if the method has no return value.
//     * Returns `null` otherwise.
//     *
//     * @return the method or constructor return value or `null`
//     *
//     * @since 1.1
//     */
//    val executableReturnValue: Any?
//
//    /**
//     * @return the property path to the value from `rootBean`
//     */
//    val propertyPath: Path?
//
//    /**
//     * Returns the value failing to pass the constraint.
//     * For cross-parameter constraints, an `Object[]` representing
//     * the method invocation arguments is returned.
//     *
//     * @return the value failing to pass the constraint
//     */
//    val invalidValue: Any?
//
//    /**
//     * Returns the constraint metadata reported to fail.
//     * The returned instance is immutable.
//     *
//     * @return constraint metadata
//     */
//    val constraintDescriptor: ConstraintDescriptor<*>?
//
//    /**
//     * Returns an instance of the specified type allowing access to
//     * provider-specific APIs. If the Jakarta Bean Validation provider
//     * implementation does not support the specified class,
//     * [ValidationException] is thrown.
//     *
//     * @param type the class of the object to be returned
//     * @param <U> the type of the object to be returned
//     * @return an instance of the specified class
//     * @throws ValidationException if the provider does not support the call
//     *
//     * @since 1.1
//    </U> */
//    fun <U> unwrap(type: Class<U>?): U
//}

// -- valiktor
///**
// * Represents a constraint violation
// *
// * @property property specifies the property that violated the constraint
// * @property value specifies the invalid value
// * @property constraint specifies the violated constraint
// * @constructor creates a constraint violation
// *
// * @author Rodolpho S. Couto
// * @see Constraint
// * @since 0.1.0
// */
//interface ConstraintViolation {
//
//    val property: String
//    val value: Any?
//    val constraint: Constraint
//}


///**
// * Represents a validation constraint
// *
// * @property name specifies the name of the constraint, generally it will be the name of the class
// * @property messageBundle specifies the base name of the default message properties file
// * @property messageKey specifies the name of the key in the message properties file
// * @property messageParams specifies the parameters to replace in the message
// *
// * @author Rodolpho S. Couto
// * @since 0.1.0
// */
//interface Constraint {
//
//    val name: String
//        get() = this.javaClass.simpleName
//
//    val messageBundle: String
//        get() = "org/valiktor/messages"
//
//    val messageKey: String
//        get() = "${this.javaClass.name}.message"
//
//    val messageParams: Map<String, *>
//        get() = this.javaClass.kotlin.declaredMemberProperties
//            .asSequence()
//            .filter {
//                Constraint::class.declaredMemberProperties
//                    .none { p -> p.name == it.name }
//            }
//            .map { it.name to it.get(this) }
//            .toMap()
//}
