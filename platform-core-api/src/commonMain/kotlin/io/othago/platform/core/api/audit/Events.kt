/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:UseSerializers(UuidSerializer::class)

package io.othago.platform.core.api.audit

import com.benasher44.uuid.Uuid
import io.othago.platform.core.api.cqrs.DataItemProto
import io.othago.platform.core.api.cqrs.PageRequest
import io.othago.platform.core.api.cqrs.PageableQueryEvent
import io.othago.platform.core.api.cqrs.QueryEvent
import io.othago.platform.core.api.serializers.UuidSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
class GetEntriesForDataItemQE(
    val dataItemType: String,
    var dataItemId: Uuid,
    override var pageRequest: PageRequest? = null,
    val dataItemProto: DataItemProto = DataItemProto.OBJ
) : PageableQueryEvent, QueryEvent()