plugins {
    kotlin("multiplatform") apply true
    kotlin("plugin.serialization") apply true
    id("maven-publish") apply true
}

val kotlinxSerializationVersion: String by project
val kotlinxCoroutinesVersion: String by project
val kotlinxCollectionsImmutableVersion: String by project
val kotlinxDatetime: String by project
val uuidVersion: String by project

kotlin {
    jvm {}
    js {
        browser {
            testTask {
                testLogging {
                    showExceptions = true
                    exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
                    showCauses = true
                    showStackTraces = true
                }
                useKarma {
                    useChromium()
                }
            }
        }
        nodejs {
            testTask {
                testLogging {
                    showExceptions = true
                    exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
                    showCauses = true
                    showStackTraces = true
                }
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                api(kotlin("stdlib-common"))
                api("org.jetbrains.kotlinx:kotlinx-serialization-core:$kotlinxSerializationVersion")
                api("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationVersion")
                api("org.jetbrains.kotlinx:kotlinx-collections-immutable:$kotlinxCollectionsImmutableVersion")
                api("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDatetime")
                api("com.benasher44:uuid:$uuidVersion")
            }
        }
        commonTest {
            dependencies {
                api(kotlin("test-common"))
                api(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                api(kotlin("stdlib-jdk8"))
            }
        }
        val jvmTest by getting {
            dependencies {
                api(kotlin("test"))
                api(kotlin("test-junit"))
            }
        }
        val jsMain by getting {
            dependencies {
                api(kotlin("stdlib-js"))
                api("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:$kotlinxCoroutinesVersion")
            }
        }
        val jsTest by getting {
            dependencies {
                api(kotlin("test-js"))
            }
        }
    }
}

publishing {
    repositories {
        mavenLocal {}
    }
    publications {
        create<MavenPublication>("maven") {
            from(components["kotlin"])
            pom {
                name.set("Othago Platform Core Api")
                description.set("Platform Api for building solutions.")
                url.set("http://othago.io/platform-core")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
            }
        }
    }
}

tasks.withType<org.gradle.jvm.tasks.Jar>().all {
    from(project.projectDir) {
        include("LICENSE.txt", "NOTICE.txt", "README.md")
        into("META-INF")
    }
}
